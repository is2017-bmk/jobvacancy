Feature: applications quantity
	As an employer
	I want to know how many applications were sent to an offer 
	In order to know if there are some applications to review

	Scenario: no applications
		Given there's an offer for a "Ruby developer"
		When nobody applied
		Then the offer has 0 applications

	Scenario: one application
		Given there's an offer for a "Ruby developer"
		When an application is sent with mail "nicopaez@gmail.com"
		Then the offer has 1 applications

	Scenario: more than one application
		Given there's an offer for a "Ruby developer"
		When an application is sent with mail "nicopaez@gmail.com"
		And an application is sent with mail "marcoklemenc@gmail.com"
		Then the offer has 2 applications
