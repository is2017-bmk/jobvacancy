class User
  include DataMapper::Resource

  property :id, Serial
  property :name, String
  property :crypted_password, String
  property :email, String
  property :is_blocked, Boolean, default: false
  property :blocked_on, DateTime
  property :failed_attempts, Integer, default: 0

  has n, :job_offers

  validates_presence_of :name
  validates_presence_of :crypted_password
  validates_presence_of :email
  validates_format_of   :email,    :with => :email_address

  def password= (password)
    self.crypted_password = ::BCrypt::Password.create(password) unless password.nil?	
  end

  def self.authenticate(email, password)
    user = User.find_by_email(email)
    if user.nil?
      return nil
    else
      if user.has_password?(password)
        user.failed_attempts = 0
        user.save
        user
      else
        add_failed_attempt(user)
        nil
      end
    end
  end

  def self.exists?(email)
    user = User.find_by_email(email)
    if user.nil?
      false
    else
      user
    end
  end

  def self.attempts_remaining(user)
    if user.is_blocked?
      0
    else
      3 - user.failed_attempts
    end
  end

  def self.add_failed_attempt(user)
    user.failed_attempts = user.failed_attempts + 1
    if user.failed_attempts == 3
      block_user(user)
    end
    user.save
  end

  def self.block_user(user)
    user.is_blocked = true
    user.blocked_on = DateTime.now
    user.failed_attempts = 0
    user.save
  end

  def self.unblock_user(user)
      user.is_blocked = false
      user.failed_attempts = 0
      user.save
  end

  def self.is_blocked?(user)
    user.is_blocked
  end

  def has_password?(password)
    ::BCrypt::Password.new(crypted_password) == password
  end

  def self.unblock_users_after_24_hours_blocked
    blocked_users = User.all(is_blocked: true)
    blocked_users.each do |user|
      if (user.blocked_on + 24.hours) <= DateTime.now
        unblock_user(user)
      end
    end
  end
end
