Feature: block account after 3 unsuccessful login attempts

	As an administrator
	I want to block an account after 3 unsuccessful login attempts
	In order to make the system more secure

    Background:
      Given there’s a user with mail "nicopaez@gmail.com" and password "Passw0rd!"
      And user "nicopaez@gmail.com" account status unlock

	Scenario: account blocked
		When I attempt a login with mail "nicopaez@gmail.com" and password "Pasw0rd!" 3 times
		Then the account with mail "nicopaez@gmail.com" is blocked
		And I attempt a login with mail "nicopaez@gmail.com" and password "Passw0rd!"
		And user "nicopaez@gmail.com" can not login

	Scenario: attempts reset upon successful login
		When I attempt a login with mail "nicopaez@gmail.com" and password "Pasw0rd!" 2 times
		And I attempt a login with mail "nicopaez@gmail.com" and password "Passw0rd!"
		Then the account with mail "nicopaez@gmail.com" has 0 unsuccessful attempts
		And the account with mail "nicopaez@gmail.com" is not blocked
		And I attempt a login with mail "nicopaez@gmail.com" and password "Passw0rd!"
		And user "nicopaez@gmail.com" can login

    Scenario: account blocked 25 hours ago
      Given I attempt a login with mail "nicopaez@gmail.com" and password "Pasw0rd!" 3 times
      And the account with mail "nicopaez@gmail.com" is blocked
      And the account with mail "nicopaez@gmail.com" was blocked 25 hours ago
      When unlock users jobs run
      Then the account with mail "nicopaez@gmail.com" is not blocked
      And I attempt a login with mail "nicopaez@gmail.com" and password "Passw0rd!"
      And user "nicopaez@gmail.com" can login

    Scenario: account blocked 23 hours ago
      Given I attempt a login with mail "nicopaez@gmail.com" and password "Pasw0rd!" 3 times
      And the account with mail "nicopaez@gmail.com" is blocked
      And the account with mail "nicopaez@gmail.com" was blocked 23 hours ago
      When unlock users jobs run
      Then the account with mail "nicopaez@gmail.com" is blocked
      And I attempt a login with mail "nicopaez@gmail.com" and password "Passw0rd!"
      And user "nicopaez@gmail.com" can not login