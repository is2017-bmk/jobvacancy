Given(/^there’s a user with mail "(.*?)" and password "(.*?)"$/) do |email, password|
  user = User.new
  user.name = "Nombre"
  user.email = email
  user.password = password
  user.save
end

When(/^I attempt a login with mail "(.*?)" and password "(.*?)" (\d+) times$/) do |email, password, times|
  visit '/login'
  times = times.to_i
  times.times do
  	visit '/login'
	fill_in('user[email]', :with => email)
	fill_in('user[password]', :with => password)
	click_button('Login')
  end
end

Then(/^the account with mail "(.*?)" is blocked$/) do |email|
  expect(User.find_by_email(email).is_blocked?).to eq true
end

Then(/^the account with mail "(.*?)" is not blocked$/) do |email|
  expect(User.find_by_email(email).is_blocked?).to eq false
end

When(/^I attempt a login with mail "(.*?)" and password "(.*?)"$/) do |email, password|
  visit '/login'
  fill_in('user[email]', :with => email)
  fill_in('user[password]', :with => password)
  click_button('Login')
end

Then(/^the account with mail "(.*?)" has (\d+) unsuccessful attempts$/) do |email, failed_attempts|
  expect(User.find_by_email(email).failed_attempts).to eq failed_attempts.to_i
end


And(/^user "([^"]*)" can login$/) do |email|
  page.should have_content(email)
end

And(/^user "([^"]*)" can not login$/) do |email|
  page.should_not have_content(email)
end

Given(/^user "([^"]*)" account status unlock$/) do |email|
  user = User.find_by_email(email)
  User.unblock_user(user)
end

When(/^unlock users jobs run$/) do
  User.unblock_users_after_24_hours_blocked
end

And(/^the account with mail "([^"]*)" was blocked (\d+) hours ago$/) do |email, hours|
  user = User.find_by_email(email)
  user.blocked_on = DateTime.now - Integer(hours).hours
  user.save
end