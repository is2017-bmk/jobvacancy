class JobOffer
  include DataMapper::Resource

  # property <name>, <type>
  property :id, Serial
  property :title, String
  property :location, String
  property :description, String
  property :years_required, Integer, default: 0
  property :submitted_applications, Integer, default: 0
  property :expiration_date, Date, default: Date.today + JobOfferExpirationPolicy.expiration_days
  property :created_on, Date
  property :updated_on, Date
  property :is_active, Boolean, default: true
  property :finalization_reason, String
  belongs_to :user

  validates_presence_of :title
  validates_numericality_of :years_required, :greater_than_or_equal_to => 0

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def self.all_active
    JobOffer.all(is_active: true)
  end

  def self.find_by_owner(user)
    JobOffer.all(user: user)
  end

  def self.find_by_owner_all_active(user)
    JobOffer.all(user: user, is_active: true)
  end

  def self.deactivate_old_offers
    active_offers = JobOffer.all(is_active: true)

    active_offers.each do |offer|
      if Date.today == offer.expiration_date
        offer.finalize("Expired")
        offer.save
      end
    end
  end

  def self.search_by_text(text)
    (JobOffer.all(:title.like => text) |
    JobOffer.all(:location.like => text) |
    JobOffer.all(:description.like => text)) &
    JobOffer.all(is_active: true)
  end

  def activate
    self.is_active = true
    self.finalization_reason = nil
  end

  def finalize(reason)
    self.is_active = false
    self.finalization_reason = reason
  end
  
  def republish
    self.is_active = true
    self.submitted_applications = 0
    self.expiration_date = Date.today + JobOfferExpirationPolicy.expiration_days
    self.finalization_reason = nil
    self.updated_on = Date.today
  end
end
