Feature: inactive offers
  As an employer
  I want to ignore inactive offers
  In order to list only those I’m hiring for

  Background:
    Given I am logged in as job offerer

  Scenario: active offer listed in my offers
    Given there's an offer for a "Python developer"
    When I browse My Offers
    Then I should see "Python developer"

  Scenario: inactive offer is not listed in my offers
    Given there's an offer for a "Cobol developer"
    And the offer is inactive
    When I browse My Offers
    Then I should not see "Cobol developer"

  Scenario: inactive offer is listed in all offers
    Given there's an offer for a "Pascal developer"
    And the offer is inactive
    When I browse View All Offers
    Then I should see "Pascal developer"
    
  Scenario: one active offer is listed and an inactive offer is not listed in my offers
    Given there's an offer for a "Pascal developer"
    And there's an offer for a "Html developer"
    And the offer is inactive
    When I browse My Offers
    Then I should see "Pascal developer"
    And I should not see "Html developer"