require 'spec_helper'

describe JobOfferYearsRequiredPolicy do
  describe 'validation of years required' do
    it 'validate zero' do
      expect(JobOfferYearsRequiredPolicy.valid_years_required?('0')).to eq true
    end

    it 'validate positive integer' do
      expect(JobOfferYearsRequiredPolicy.valid_years_required?('20')).to eq true
    end

    it 'dont validate float' do
      expect(JobOfferYearsRequiredPolicy.valid_years_required?('20.3')).to eq false
    end

    it 'dont validate invalid numeric format' do
      expect(JobOfferYearsRequiredPolicy.valid_years_required?('a')).to eq false
    end

    it 'dont validate nil' do
      expect(JobOfferYearsRequiredPolicy.valid_years_required?(nil)).to eq false
    end
  end
end
