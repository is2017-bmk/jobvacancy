Given(/^I have made a search and gotten some results$/) do
  @job_offer_one = JobOffer.new
  @job_offer_one.owner = User.first
  @job_offer_one.title = "Example 1"
  @job_offer_one.location = 'a nice job'
  @job_offer_one.description = 'a nice job'
  @job_offer_one.save
  @job_offer_two = JobOffer.new
  @job_offer_two.owner = User.first
  @job_offer_two.title = "Example 2"
  @job_offer_two.location = 'another nice job'
  @job_offer_two.description = 'another nice job'
  @job_offer_two.save
  visit '/job_offers'
  fill_in('q', :with => 'another')
  click_button('search')
end

When(/^I clear the search$/) do
  visit '/job_offers'
end

Then(/^I get all offers$/) do
  page.should have_content(@job_offer_one.title)
  page.should have_content(@job_offer_two.title)
  @job_offer_one.destroy
  @job_offer_two.destroy
end