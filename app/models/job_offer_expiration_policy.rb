# Expiration date policy for job offers
class JobOfferExpirationPolicy
  def self.expiration_days
    30
  end

  def self.valid_expiration_date?(date)
    (Date.parse date rescue false) && ((Date.parse date) >= Date.today)
  end
end


