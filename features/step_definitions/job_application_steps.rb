Given(/^only a "(.*?)" offer exists in the offers list$/) do | job_title |
  JobOffer.all.destroy
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = job_title
  @job_offer.location = 'a nice job'
  @job_offer.description = 'a nice job'
  @job_offer.save
end

Given(/^I access the offers list page$/) do
  visit '/job_offers'
end

When(/^I apply$/) do
  email = 'applicant@test.com'
  short_bio = 'blah'
  remuneration = 1000
  @application = JobApplication.create_for(email, @job_offer, short_bio, intended_remuneration=remuneration)
  @application.process
end

Then(/^I should receive a mail with offerer info$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
end

Given(/^there's an offer for a "([^"]*)"$/) do |offer_title|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = offer_title
  @job_offer.save
end

When(/^I filter max remuneration by "([^"]*)"$/) do |max_remuneration|
  fill_in('maxrem', :with => max_remuneration)
  click_button('filter-button')
end

When(/^an application is sent with email "([^"]*)" and short bio "([^"]*)" at "([^"]*)" with intended remuneration "([^"]*)"$/) do |email, short_bio, datetime, intended_remuneration|
  @application = JobApplication.create_for(email, @job_offer, short_bio, datetime, intended_remuneration)
end

When(/^an application is sent with email "([^"]*)" and short bio "([^"]*)" at "([^"]*)"$/) do |email, short_bio, datetime|
  @application = JobApplication.create_for(email, @job_offer, short_bio, datetime, intended_remuneration=1000)
end

Then(/^the application's date and time, mail, short bio and intended remuneration are stored$/) do
  @application.saved?.should eq TRUE
end

When(/^an application is sent with email "([^"]*)" at "([^"]*)" with intended remuneration "([^"]*)"$/) do |email, datetime, intended_remuneration|
  @application = JobApplication.create_for(email, @job_offer, NIL, datetime, intended_remuneration)
end

Then(/^nothing about the application is stored$/) do
  @application.saved?.should eq FALSE
end

When(/^an application is sent with short bio "([^"]*)" at "([^"]*)"$/) do |short_bio, datetime|
  @application = JobApplication.create_for(NIL, @job_offer, short_bio, datetime)
end

And(/^an application was sent with mail "([^"]*)"$/) do |mail|
  @application = JobApplication.create_for(mail, @job_offer, "blah", intended_remuneration=1000)
end

When(/^I get all the applications for the offer$/) do
  visit '/job_applications/offer/%s' % @job_offer.id
end

Then(/^"([^"]*)" should be listed$/) do |mail|
  expect(page.current_path).to eq('/job_applications/offer/%s' % @job_offer.id)
  page.should have_content(mail)
end

And(/^an application was sent with mail "([^"]*)" for other offer "([^"]*)"$/) do |mail, offer_title|
  other_offer = JobOffer.new
  other_offer.owner = User.first
  other_offer.title = offer_title
  other_offer.save
  JobApplication.create_for(mail, other_offer, "blah", intended_remuneration=1000)
end

Then(/^"([^"]*)" should not be listed$/) do |mail|
  expect(page.current_path).to eq('/job_applications/offer/%s' % @job_offer.id)
  page.should_not have_content(mail)
end

When(/^nobody applied$/) do
  # no hacer nada
end

Then(/^the offer has (\d+) applications$/) do |number_of_applications|
  @job_offer.submitted_applications.should be == number_of_applications.to_i
end

When(/^an application is sent with mail "(.*?)"$/) do |email|
  @application = JobApplication.create_for(email, @job_offer, "", intended_remuneration=1000)
end

Then(/^I get the application with email "(.*?)"$/) do |email|
  page.should have_content(email)
end

Then(/^I dont get the application with email "([^"]*)"$/) do |email|
  page.should_not have_content(email)
end