JobVacancy::App.controllers :sessions do
  
  get :login, :map => '/login' do
    @user = User.new
    render 'sessions/new'
  end

  post :create do
    email = params[:user][:email]
    password = params[:user][:password]
    @user = User.exists?(email)
    if (@user == false)
      @user = User.new
      flash.now[:error] = 'Invalid credentials'
      render 'sessions/new'
    else
      if @user.is_blocked?
        @user = User.new
        flash.now[:error] = 'Your user has been blocked for 24 hours'
        render 'sessions/new'
      else
        @user = User.authenticate(email, password)
        if @user.nil?
          @user = User.exists?(email)
          attempts_remaining = User.attempts_remaining(@user)
          if attempts_remaining == 0
            @user = User.new
            flash.now[:error] = 'Login failed, wrong password. For security reasons your user has been blocked for 24 hours'
            render 'sessions/new'
          else
            message = 'Login failed, wrong password. You have ' + attempts_remaining.to_s + ' attempts remaining'
            @user = User.new
            flash.now[:error] = message
            render 'sessions/new'
          end
        else
          sign_in @user
          redirect '/'  
        end        
      end
    end
  end

  get :destroy, :map => '/logout' do 
    sign_out
    redirect '/'          
  end

end
