migration 9, :add_years_required_to_job_offers do
  up do
    modify_table :job_offers do
      add_column :years_required, Integer, :default => 0
    end
  end

  down do
    modify_table :job_offers do
      drop_column :years_required
    end
  end
end
