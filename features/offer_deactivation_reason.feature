Feature: offer finalization reason
	As an employer
	I want to know the reason why an offer was finalized
	In order to separate hirings from cancellations and expirations

	Scenario: finalized by expiration
	    Given there's an offer for a "Ruby developer"
	    And offer expiration date is 31 days ago
	    When offer expiration job run
	    Then the reason why the offer was finalized should be "Expired"
	
	Scenario: finalized by cancellation
	    Given there's an offer for a "Ruby developer"
	    When the offer is cancelled
	    Then the reason why the offer was finalized should be "Canceled"

	Scenario: finalized by inside hiring
	    Given there's an offer for a "Ruby developer"
	    When I hire someone from inside the app
	    Then the reason why the offer was finalized should be "Internal Applicant Hired"

	Scenario: finalized by outside hiring
	    Given there's an offer for a "Ruby developer"
	    When I hire someone from outside the app
	    Then the reason why the offer was finalized should be "External Applicant Hired"