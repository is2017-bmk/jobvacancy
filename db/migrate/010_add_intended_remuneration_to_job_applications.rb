migration 10, :add_intended_remuneration_to_job_applications do
  up do
    modify_table :job_applications do
      add_column :intended_remuneration, Float
    end
  end

  down do
    modify_table :job_applications do
      drop_column :intended_remuneration
    end
  end
end