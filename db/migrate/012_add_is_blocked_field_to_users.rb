migration 12, :add_is_blocked_field_to_users do
  up do
  	 modify_table :users do
      add_column :is_blocked, TrueClass, :default => false
    end
  end

  down do
  	modify_table :users do
      drop_column :is_blocked
    end
  end
end
