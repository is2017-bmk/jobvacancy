class JobApplication
  include DataMapper::Resource

  # property <name>, <type>
  property :id, Serial
  property :created_on, DateTime
  property :applicant_email, String
  property :applicant_short_bio, Text
  property :intended_remuneration, Float

  belongs_to :job_offer

  validates_presence_of :applicant_email
  validates_presence_of :applicant_short_bio
  validates_presence_of :intended_remuneration
  validates_numericality_of :intended_remuneration, :greater_than => 0

  def self.create_for(email, offer, short_bio, created_on = NIL, intended_remuneration)
    app = JobApplication.new
    app.applicant_email = email
    app.job_offer = offer
    app.applicant_short_bio = short_bio
    app.created_on = created_on || DateTime.now
    app.intended_remuneration = intended_remuneration
    app.save
    offer.submitted_applications += 1
    offer.save
    app
  end

  def self.find_by_offer(offer, order)
    if order == "desc"
      JobApplication.all(:job_offer => offer, :order => [ :intended_remuneration.desc ])
    else
      JobApplication.all(:job_offer => offer, :order => [ :intended_remuneration.asc ])
    end
  end

  def self.find_by_offer_and_maxrem(offer, maxrem, order)
    if order == "desc"
      JobApplication.all(:job_offer => offer, :intended_remuneration.lte => maxrem, :order => [ :intended_remuneration.desc ])
    else
      JobApplication.all(:job_offer => offer, :intended_remuneration.lte => maxrem, :order => [ :intended_remuneration.asc ])
    end
  end

  def destroy
    @job_offer.submitted_applications -= 1
    @job_offer.save
    super
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
  end
end
