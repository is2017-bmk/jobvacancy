require 'spec_helper'

describe JobApplication do
  describe 'model' do
    subject { @job_offer = JobApplication.new }

    it { should respond_to(:applicant_email) }
    it { should respond_to(:applicant_short_bio) }
    it { should respond_to(:job_offer) }
    it { should respond_to(:created_on) }
    it { should respond_to(:intended_remuneration) }
  end

  describe 'create_for' do
    it 'should set applicant_email' do
      email = 'applicant@test.com'
      ja = JobApplication.create_for(email, JobOffer.new, 'blah', intended_remuneration=1000)
      ja.applicant_email.should eq email
    end

    it 'should set job_offer' do
      offer = JobOffer.new
      ja = JobApplication.create_for('applicant@test.com', offer, 'blah', intended_remuneration=1000)
      ja.job_offer.should eq offer
    end

    it 'should set applicant_short_bio' do
      short_bio = 'blah'
      ja = JobApplication.create_for('applicant@test.com', JobOffer.new, short_bio, intended_remuneration=1000)
      ja.applicant_short_bio.should eq short_bio
    end

    it 'should set intended_remuneration' do
      short_bio = 'blah'
      ja = JobApplication.create_for('applicant@test.com', JobOffer.new, short_bio, intended_remuneration=1000)
      ja.intended_remuneration.should eq 1000
    end

    it 'should increase applications number' do
      short_bio = 'blah'
      offer = JobOffer.new
      ja = JobApplication.create_for('applicant@test.com', offer, short_bio, intended_remuneration=1000)
      offer.submitted_applications.should eq 1
    end
  end

  describe 'destroy' do
    it 'should decrease applications number' do
      short_bio = 'blah'
      offer = JobOffer.new
      ja = JobApplication.create_for('applicant@test.com', offer, short_bio, intended_remuneration=1000)
      ja.destroy
      offer.submitted_applications.should eq 0
    end
  end

  describe 'process' do
    let(:job_application) { JobApplication.new }

    it 'should deliver contact info notification' do
      ja = JobApplication.create_for('applicant@test.com', JobOffer.new, 'blah', intended_remuneration=1000)
      JobVacancy::App.should_receive(:deliver).with(:notification, :contact_info_email, ja)
      ja.process
    end
  end
end
