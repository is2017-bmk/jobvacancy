require 'spec_helper'

describe JobOfferExpirationPolicy do

  describe 'validation of expiration_date' do
    it 'validate future date' do
      expect(JobOfferExpirationPolicy.valid_expiration_date?((Date.today + 1.days).to_s)).to eq true
    end

    it 'validate current date' do
      expect(JobOfferExpirationPolicy.valid_expiration_date?((Date.today).to_s)).to eq true
    end

    it 'dont validate passed date' do
      expect(JobOfferExpirationPolicy.valid_expiration_date?((Date.today - 1.days).to_s)).to eq false
    end

    it 'dont validate invalid date format' do
      expect(JobOfferExpirationPolicy.valid_expiration_date?("a")).to eq false
    end

    it 'dont validate nil' do
      expect(JobOfferExpirationPolicy.valid_expiration_date?(nil)).to eq false
    end
  end
end
