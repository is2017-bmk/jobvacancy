JobVacancy::App.controllers :job_applications do

  get :offer, :with =>:offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    @applications = JobApplication.find_by_offer(@job_offer, params[:order])
    render 'job_applications/list'
  end

  post :offer, :with =>:offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    @applications = JobApplication.find_by_offer(@job_offer, params[:order])
    if params[:maxrem] != ''
      begin
        maximum_intended_renumeration = Float(params[:maxrem])
        @applications = JobApplication.find_by_offer_and_maxrem(@job_offer, maximum_intended_renumeration, params[:order])
      rescue ArgumentError
        flash.now[:error] = "Invalid max remuneration"
      end
    end
    render 'job_applications/list'
  end

end
