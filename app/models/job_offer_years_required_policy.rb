# Years required policy for job offers
class JobOfferYearsRequiredPolicy

  def self.valid_years_required?(years_required)
    (Float(years_required) rescue false) &&
        !Float(years_required).nil? &&
        (years_required.to_f >= 0) &&
        (years_required.to_f % 1 == 0.0)
  end
end


