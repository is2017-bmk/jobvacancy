When(/^I browse the default page$/) do
  visit '/'
end

Given(/^I am logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', :with => 'offerer@test.com')
  fill_in('user[password]', :with => 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^I access the new offer page$/) do
  visit '/job_offers/new'
  page.should have_content('Title')
end

When(/^I fill the title with "(.*?)"$/) do |offer_title|
  fill_in('job_offer[title]', :with => offer_title)
end

When(/^confirm the new offer$/) do
  click_button('Create')
end

Then(/^I should see "(.*?)" in My Offers$/) do |content|
	visit '/job_offers/my'
  page.should have_content(content)
end

Then(/^I should see "(.*?)" in View All Offers$/) do |content|
  visit '/job_offers/myall'
  page.should have_content(content)
end

Then(/^I should not see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

Given(/^I have "(.*?)" offer in My Offers$/) do |offer_title|
  JobOffer.all.destroy
  visit '/job_offers/new'
  fill_in('job_offer[title]', :with => offer_title)
  click_button('Create')
end

Given(/^I edit it$/) do
  click_link('Edit')
end

And(/^I delete it$/) do
  click_button('Delete')
end

Given(/^I set title to "(.*?)"$/) do |new_title|
  fill_in('job_offer[title]', :with => new_title)
end

Given(/^I save the modification$/) do
  click_button('Save')
end

Given(/^there's an offer for "(.*?)" in "(.*?)" with description "(.*?)"$/) do |title, location, description|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.location = location
  @job_offer.description = description
  @job_offer.save
end

When(/^I create an offer for a "(.*?)" with (\d+) years of experience$/) do |title, years_required|
  fill_in('job_offer[title]', :with => title)
  fill_in('job_offer[years_required]', :with => years_required)
end

When(/^I create an offer for a "(.*?)" with "(.*?)" years of experience$/) do |title, years_required|
  fill_in('job_offer[title]', :with => title)
  fill_in('job_offer[years_required]', :with => years_required)
end

Then(/^the offer should be saved with "(.*?)" years of experience$/) do |years_required|
  click_button('Create')
end

Then(/^"(.*?)" should be indicated each time the offer is listed$/) do |years_required| 
  visit '/job_offers/latest'
  page.should have_content(years_required)
end

Then(/^the offer should be saved with (\d+) years of experience$/) do |years_required|
  click_button('Create')
  visit '/job_offers/latest'
  page.should have_content(years_required)
end

When(/^I create an offer for a "(.*?)"$/) do |title|
  fill_in('job_offer[title]', :with => title)
end

Then(/^"(.*?)" years should be present each time the offer is listed$/) do |years_required|
  visit '/job_offers/latest'
  page.should have_content(years_required)
end

Then(/^​"(.*?)"​ years should be present each time the offer is listed$/) do |years_required|
  visit '/job_offers/latest'
  page.should have_content(years_required)
end

Then(/^the offer should not be saved$/) do
  click_button('Create')
end

Then(/^​I receive the error "(.*?)"$/) do |error|
  page.should have_content(error)
end

And(/^the offer is inactive$/) do
  @job_offer.finalize("Reason")
  @job_offer.save
end

When(/^I browse My Offers$/) do
  visit '/job_offers/my'
end

When(/^I browse View All Offers$/) do
  visit '/job_offers/myall'
end

And(/^I fill the expiration date with "([^"]*)"$/) do |expiration_date|
  fill_in('job_offer[expiration_date]', :with => expiration_date)
end

When(/^deactivate offers jobs run$/) do
  JobOffer.deactivate_old_offers
end

Given(/^there's an offer for a "([^"]*)" with a passed expiration date$/) do |title|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.years_required = 1
  @job_offer.expiration_date = Date.today - 1.days
  @job_offer.save
end

Given(/^there's an offer for a "([^"]*)" with future expiration date$/) do |title|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.years_required = 1
  @job_offer.save
end

And(/^I fill the expiration date with a future date$/) do
  fill_in('job_offer[expiration_date]', :with => Date.today + 1.days)
end

And(/^I fill the expiration date with current date$/) do
  fill_in('job_offer[expiration_date]', :with => Date.today)
end

Given(/^I have "([^"]*)" offer in My Offers with future expiration date$/) do |title|
  JobOffer.all.destroy
  visit '/job_offers/new'
  fill_in('job_offer[title]', :with => title)
  fill_in('job_offer[expiration_date]', :with => Date.today + 1.days)
  click_button('Create')
end

Given(/^I have "([^"]*)" offer in My Offers with passed expiration date$/) do |title|
  JobOffer.all.destroy
  visit '/job_offers/new'
  fill_in('job_offer[title]', :with => title)
  fill_in('job_offer[expiration_date]', :with => Date.today - 1.days)
  click_button('Create')
end

Given(/^I have "([^"]*)" offer in My Offers with current expiration date$/) do |title|
  JobOffer.all.destroy
  visit '/job_offers/new'
  fill_in('job_offer[title]', :with => title)
  fill_in('job_offer[expiration_date]', :with => Date.today)
  click_button('Create')
end

Then(/^the reason why the offer was finalized should be "(.*?)"$/) do |arg1|
  @job_offer.finalization_reason.should == arg1
end

When(/^the offer is cancelled$/) do
  @job_offer.finalize("Canceled")
  @job_offer.save
end

When(/^I hire someone from inside the app$/) do
  @job_offer.finalize("Internal Applicant Hired")
  @job_offer.save
end

When(/^I hire someone from outside the app$/) do
  @job_offer.finalize("External Applicant Hired")
  @job_offer.save
end

Given(/^offer expiration date is (\d+) days ago$/) do |arg1|
  pending # express the regexp above with the code you wish you had
end

When(/^offer expiration job run$/) do
  pending # express the regexp above with the code you wish you had
end

Given(/^the offer is "(.*?)"$/) do |finalization_reason|
  @job_offer.finalize(finalization_reason.to_s)
  @job_offer.save
end

When(/^the offer is republished$/) do
  @job_offer.republish
  @job_offer.save
end

Then(/^the offer should not be "(.*?)"$/) do |finalization_reason|
  expect(@job_offer.finalization_reason == finalization_reason.to_s).to be_falsey
end

Then(/^the offer’s expiration date should be (\d+) days from today$/) do |days|
  expect(@job_offer.expiration_date == Date.today + days.to_i).to be_truthy
end

Given(/^there's just an offer for a "(.*?)"$/) do |offer_title|
  JobOffer.all.destroy
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = offer_title
  @job_offer.save
end