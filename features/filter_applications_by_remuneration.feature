Feature: filter applications by max remuneration
  As an offerer
  I want to filter applications  by max remuneration
  In order to discard those out of my range
  
  Scenario: filter one application with renumeration 100 and other for 200 by remuneration 150
    Given there's an offer for a "Ruby developer"
    And an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "100"
    And an application is sent with email "baldileandro@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "200"
    When I get all the applications for the offer
    And I filter max remuneration by "150"
    Then I get the application with email "nicopaez@gmail.com"

  Scenario: filter one application with renumeration 100 and other for 200 by remuneration 300
    Given there's an offer for a "Ruby developer"
    And an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "100"
    And an application is sent with email "baldileandro@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "200"
    When I get all the applications for the offer
    And I filter max remuneration by "300"
    Then I get the application with email "baldileandro@gmail.com"
    And  I get the application with email "nicopaez@gmail.com"

  Scenario: filter one application with renumeration 100 and other for 200 by remuneration 0
    Given there's an offer for a "Ruby developer"
    And an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "100"
    And an application is sent with email "baldileandro@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "200"
    When I get all the applications for the offer
    And I filter max remuneration by "0"
    Then I dont get the application with email "baldileandro@gmail.com"
    And  I dont get the application with email "nicopaez@gmail.com"

  Scenario: filter one application with renumeration 100 and other for 200 by remuneration 100
    Given there's an offer for a "Ruby developer"
    And an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "100"
    And an application is sent with email "baldileandro@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "200"
    When I get all the applications for the offer
    And I filter max remuneration by "100"
    Then I dont get the application with email "baldileandro@gmail.com"
    And  I get the application with email "nicopaez@gmail.com"