Feature: Get applications for an offer
  As an employer
  I want to see all the applications made for a particular offer
  In order to see and contact the applicants

  Scenario: get applications for an offer with one application
    Given there's an offer for a "Ruby developer"
    And an application was sent with mail "nicopaez@gmail.com"
    When I get all the applications for the offer
    Then "nicopaez@gmail.com" should be listed

  Scenario: get applications for an offer with two application
    Given there's an offer for a "Ruby developer"
    And an application was sent with mail "nicopaez@gmail.com"
    And an application was sent with mail "baldileandro@gmail.com"
    When I get all the applications for the offer
    Then "nicopaez@gmail.com" should be listed
    And "baldileandro@gmail.com" should be listed

  Scenario: get applications for an offer a
    Given there's an offer for a "Ruby developer"
    And an application was sent with mail "nicopaez@gmail.com"
    And an application was sent with mail "baldileandro@gmail.com" for other offer "Java developer"
    When I get all the applications for the offer
    Then "baldileandro@gmail.com" should not be listed
    And "nicopaez@gmail.com" should be listed
