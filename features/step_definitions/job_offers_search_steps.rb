Given(/^there’s an offer for "(.*?)" in "(.*?)" with description "(.*?)"$/) do |title, location, description|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.location = location
  @job_offer.description = description
  @job_offer.save
end

When(/^I search "(.*?)"$/) do |word|
  visit '/job_offers/latest'
  fill_in('q', :with => word)
  click_button('search-button')
end

Then(/^I get the offer with title "(.*?)"$/) do |title|
  page.should have_content(title)
end

