require 'spec_helper'

describe JobOffer do

  describe 'model' do
    subject { @job_offer = JobOffer.new }

    it { should respond_to(:id) }
    it { should respond_to(:title) }
    it { should respond_to(:location) }
    it { should respond_to(:description) }
    it { should respond_to(:owner) }
    it { should respond_to(:owner=) }
    it { should respond_to(:created_on) }
    it { should respond_to(:updated_on) }
    it { should respond_to(:expiration_date) }
    it { should respond_to(:finalization_reason) }
    it { should respond_to(:is_active) }
  end

  describe 'valid?' do
    let(:job_offer) do
      user = User.new
      user.id = 1
      job_offer = JobOffer.new
      job_offer.owner = user
      job_offer
    end

    it 'should be false when title is blank' do
      expect(job_offer.valid?).to eq false
    end

    it 'should be true when title is not blank' do
      job_offer.title = 'the title'
      expect(job_offer.valid?).to eq true
    end

    it 'should be false when years required is not numeric' do
      job_offer.title = 'Title'
      job_offer.years_required = 'fake'
      expect(job_offer.valid?).to be_falsey
    end

    it 'should be true when years required not numeric' do
      job_offer.title = 'Title'
      job_offer.years_required = 5
      expect(job_offer.valid?).to be_truthy
    end

    it 'should be true when years is 0' do
      job_offer.title = 'Title'
      job_offer.years_required = 0
      expect(job_offer.valid?).to be_truthy
    end

    it 'should be false when years required is less than 0' do
      job_offer.title = 'Title'
      job_offer.years_required = -1
      expect(job_offer.valid?).to be_falsey
    end
  end

  describe 'activate' do
	let(:job_offer) do
	  job_offer = JobOffer.new
	end

	it 'offer should be active' do
	  job_offer.finalize("Canceled")
	  job_offer.activate
	  expect(job_offer.is_active).to eq true
	end

	it 'offer finalization reason should be empty' do
	  job_offer.finalize("Canceled")
	  job_offer.activate
	  expect(job_offer.finalization_reason).to eq nil
	end
  end

  describe 'finalize' do
	let(:job_offer) do
		job_offer = JobOffer.new
	  end

	  it 'offer should be active' do
		job_offer.finalize("Canceled")
		expect(job_offer.is_active).to eq false
	  end

	  it 'offer finalization reason should be empty' do
		job_offer.finalize("Canceled")
		expect(job_offer.finalization_reason).to eq "Canceled"
	  end
  end

  describe 'deactive_old_offers' do
    let(:today_offer) do
      today_offer = JobOffer.new
      today_offer.expiration_date = Date.today + JobOfferExpirationPolicy.expiration_days
      today_offer
    end

    let(:thirty_day_offer) do
      thirty_day_offer = JobOffer.new
      thirty_day_offer.expiration_date = Date.today
      thirty_day_offer
    end

    it 'should deactivate offers updated 45 days ago' do
      JobOffer.should_receive(:all).and_return([thirty_day_offer])
      JobOffer.deactivate_old_offers
      expect(thirty_day_offer.is_active).to eq false
    end

    it 'should not deactivate offers created today' do
      JobOffer.should_receive(:all).and_return([today_offer])
      JobOffer.deactivate_old_offers
      expect(today_offer.is_active).to eq true
    end
  end

  describe 'years required by default' do
    let(:job_offer) do
      user = User.new
      user.id = 1
      job_offer = JobOffer.new
      job_offer.owner = user
      job_offer
    end

    it 'should set the offer with 0 years of experience required by default when the field is not setted' do
      job_offer.title = 'Title'
      expect(job_offer.years_required).to eq 0
    end
  end

  describe 'expiration date behaviour' do
    let(:job_offer) do
      job_offer = JobOffer.new
    end

    it 'should set the offer expiration date with expiration date policy days after current by default when the field is not setted' do
      expect(job_offer.expiration_date).to eq (Date.today + JobOfferExpirationPolicy.expiration_days)
    end
  end

    describe 'republish_offers' do

		let(:expired_offer) do
			expired_offer = JobOffer.new
			expired_offer.updated_on = Date.today - 45
			expired_offer.is_active = false
			expired_offer.finalization_reason = 'Expired'
			expired_offer.submitted_applications = 5
			expired_offer
		end

		it 'should activate a republished offer' do
			expired_offer.republish
			expect(expired_offer.is_active).to be_truthy
		end

		it 'should set submitted applications to 0 a when an offer is republished' do
			expired_offer.republish
			expect(expired_offer.submitted_applications).to eq 0
		end

		it 'should refresh the update_on time of the offer' do
			expired_offer.republish
			expect(expired_offer.updated_on).to eq Date.today
		end

		it 'should update the expiration date of the offer to today+30days' do
			expired_offer.republish
			expect(expired_offer.expiration_date).to eq Date.today + 30 
		end

		it 'should set to nil the finalization reason of the offer' do
			expired_offer.republish
			expect(expired_offer.finalization_reason).to eq nil
		end
	end

end
