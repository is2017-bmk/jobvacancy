Feature: case-insensitive search across all fields
  As an applicant
  I want to have an offer search that searches across all fields (title, location   and description) and is case-insensitive
  In order to have an easier time finding offers well suited for me
  
  Scenario: title search
    Given there’s an offer for "Ruby Developer" in "El Palomar" with description "Padrino Applications"
    When I search "ruby"
    Then I get the offer with title "Ruby Developer"

  Scenario: location search
    Given there’s an offer for "Ruby Developer" in "El Palomar" with description "Padrino Applications"
    When I search "palomar"
    Then I get the offer with title "Ruby Developer"

  Scenario: description search
    Given there’s an offer for "Ruby Developer" in "El Palomar" with description "Padrino Applications"
    When I search "padrino"
    Then I get the offer with title "Ruby Developer"

  Scenario: description search
    Given there’s an offer for "Ruby Developer" in "El Palomar" with description "Padrino Applications"
    When I search "PadrINo"
    Then I get the offer with title "Ruby Developer"

  Scenario: description search
    Given there’s an offer for "Ruby Developer" in "El Palomar" with description "Padrino Applications"
    When I search "el     palomar"
    Then I get the offer with title "Ruby Developer"

  Scenario: description search
    Given there’s an offer for "Ruby Developer" in "El Palomar" with description "Padrino Applications"
    When I search " padrino "
    Then I get the offer with title "Ruby Developer"