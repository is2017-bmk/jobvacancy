Feature: clear search
	As an applicant
	I want to be able to clear the search results and filter
	In order to be able to make another search easily

	Scenario: clear search
	  Given I have made a search and gotten some results
	  When I clear the search
	  Then I get all offers

