migration 13, :add_blocked_on_field_to_users do
  up do
  	 modify_table :users do
      add_column :blocked_on, DateTime
    end
  end

  down do
  	modify_table :users do
      drop_column :blocked_on
    end
  end
end