Feature: Years of Experience
  As an employer
  I want to indicate how many years of experience are required in an offer
  In order to filter unfit applicants more easily
  
  Background:
    Given I am logged in as job offerer

  Scenario: offer with years of experience
    Given I access the new offer page
    When I create an offer for a "Ruby developer" with "2" years of experience
    Then the offer should be saved with "2" years of experience
    And "2" years should be present each time the offer is listed

  Scenario: offer without years of experience
    Given I access the new offer page
    When I create an offer for a "Ruby developer"
    Then the offer should be saved with "0" years of experience
    And ​"No experience"​ years should be present each time the offer is listed

  Scenario: offer with years of experience not numeric
    Given I access the new offer page
    When I create an offer for a "Ruby developer" with "fake" years of experience
    Then the offer should not be saved
    And ​I receive the error "Years required must be numeric and greater or equal than zero"

  Scenario: offer with years of experience negative
    Given I access the new offer page
    When I create an offer for a "Ruby developer" with "-3" years of experience
    Then the offer should not be saved
    And ​I receive the error "Years required must be numeric and greater or equal than zero"

  Scenario: offer with years of experience not integer
    Given I access the new offer page
    When I create an offer for a "Ruby developer" with "3.5" years of experience
    Then the offer should not be saved
    And ​I receive the error "Years required must be numeric and greater or equal than zero"