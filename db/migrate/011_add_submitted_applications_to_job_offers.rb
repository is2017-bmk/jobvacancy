migration 11, :add_submitted_applications_to_job_offers do
  up do
    modify_table :job_offers do
      add_column :submitted_applications, Integer, :default => 0
    end
  end

  down do
    modify_table :job_offers do
      drop_column :submitted_applications
    end
  end
end
