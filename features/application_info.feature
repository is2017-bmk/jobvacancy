Feature: application info
  As an employer
  I want to gather, and store in the system, information about applications
  In order to be able to access it later

  Scenario: application with short bio
    Given there's an offer for a "Ruby developer"
  	When an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "1000"
    Then the application's date and time, mail, short bio and intended remuneration are stored

  Scenario: application without short bio
    Given there's an offer for a "Ruby developer"
  	When an application is sent with email "nicopaez@gmail.com" at "2017/10/25 18:00:00" with intended remuneration "1000"
  	Then nothing about the application is stored
  
  Scenario: application without email
    Given there's an offer for a "Ruby developer"
  	When an application is sent with short bio "Hello" at "2017/10/25 18:00:00"
  	Then nothing about the application is stored

  Scenario: application without intended remuneration
    Given there's an offer for a "Ruby developer"
    When an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration ""
    Then nothing about the application is stored

  Scenario: application without intended remuneration not numeric
    Given there's an offer for a "Ruby developer"
    When an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "fake"
    Then nothing about the application is stored

  Scenario: application without intended remuneration negative
    Given there's an offer for a "Ruby developer"
    When an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "-5"
    Then nothing about the application is stored

      Scenario: application without intended remuneration = 0
    Given there's an offer for a "Ruby developer"
    When an application is sent with email "nicopaez@gmail.com" and short bio "Hello" at "2017/10/25 18:00:00" with intended remuneration "0"
    Then nothing about the application is stored