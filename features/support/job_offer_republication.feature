Feature: offer republication
	As an employer
	I want to republish an expired or canceled offer
	In order to keep looking for applicants

  	Background:
    	Given I am logged in as job offerer

	Scenario: republish canceled offer
		Given there's an offer for a "Ruby developer"
		And the offer is "Expired"
		When the offer is republished
		Then the offer should not be "Expired"
		And the offer’s expiration date should be 30 days from today

    Scenario: a canceled offer should not be able to republish
    	Given there's just an offer for a "Pascal developer"
		And the offer is "Canceled"
    	When I browse View All Offers
    	Then I should not see "Republish"

  	Scenario: a expired offer should be able to republish
    	Given there's just an offer for a "A cobol developer"
		And the offer is "Expired"
    	When I browse View All Offers
    	Then I should see "Republish"