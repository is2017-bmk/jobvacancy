require 'recaptcha'

JobVacancy::App.controllers :job_offers do

  include Recaptcha::ClientHelper
  include Recaptcha::Verify

  get :my do
    @offers = JobOffer.find_by_owner_all_active(current_user)
    render 'job_offers/my_offers'
  end

  get :myall do
    @offers = JobOffer.find_by_owner(current_user)
    render 'job_offers/my_offers'
  end

  get :finalize, :with =>:offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    render 'job_offers/finalize'
  end

  post :finalize, :with =>:offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_offer.finalize(params[:job_offer][:finalization_reason])
    if @job_offer.save
      flash[:success] = 'Offer finalized'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Operation failed'
      redirect '/job_offers/my'
    end
  end

  get :index do
    @offers = JobOffer.all_active
    render 'job_offers/list'
  end  

  get :new do
    @job_offer = JobOffer.new
    render 'job_offers/new'
  end

  get :latest do
    @offers = JobOffer.all_active
    render 'job_offers/list'
  end

  get :edit, :with =>:offer_id  do
    @job_offer = JobOffer.get(params[:offer_id])
    # ToDo: validate the current user is the owner of the offer
    render 'job_offers/edit'
  end

  get :apply, :with =>:offer_id  do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_application = JobApplication.new
    # ToDo: validate the current user is the owner of the offer
    render 'job_offers/apply'
  end

  post :search do
    if params[:search_button]
      @q = "#{params[:q]}"
      @params = @q.strip
      @params.gsub!(/\s+/, ' ')
      @params = '%'+@params+'%'

      @offers = JobOffer.search_by_text(@params)
    else
      @offers = JobOffer.all_active()
    end
    render 'job_offers/list'
  end

  post :apply, :with => :offer_id do
    applicant_email = params[:job_application][:applicant_email]
    applicant_short_bio = params[:job_application][:applicant_short_bio]
    intended_remuneration = params[:job_application][:intended_remuneration]
    @job_offer = JobOffer.get(params[:offer_id]) 
    if verify_recaptcha
      if intended_remuneration.to_f > 0.0
        @job_application = JobApplication.create_for(applicant_email, @job_offer, applicant_short_bio, intended_remuneration)
        @job_application.process
        flash[:success] = 'Application accepted.'
        redirect '/job_offers'
      else
        flash.now[:error] = 'Intended remuneration must be numeric, and greater than zero.'
        @job_offer = JobOffer.get(params[:offer_id])
        @job_application = JobApplication.new
        @job_application.applicant_email = applicant_email
        @job_application.applicant_short_bio = applicant_short_bio
        @job_application.intended_remuneration = intended_remuneration
        render 'job_offers/apply'  
      end
    else
      flash.now[:error] = 'Invalid captcha.'
      @job_application = JobApplication.new
      @job_application.applicant_email = applicant_email
      @job_application.applicant_short_bio = applicant_short_bio
      @job_application.intended_remuneration = intended_remuneration
      render 'job_offers/apply'  
    end
  end

  post :create do
    @job_offer = JobOffer.new(params[:job_offer])
    @job_offer.owner = current_user
    error = nil

    expiration_date = params[:job_offer][:expiration_date]
    unless JobOfferExpirationPolicy.valid_expiration_date?(expiration_date)
      error = 'Invalid expiration date. Must be greater equal than current date'
    end

    years_required = params[:job_offer][:years_required]
    unless JobOfferYearsRequiredPolicy.valid_years_required?(years_required)
      error = 'Years required must be numeric and greater or equal than zero'
    end

    if error
      flash.now[:error] = error
      render 'job_offers/new'
    else
      if @job_offer.save
        if params['create_and_twit']
          TwitterClient.publish(@job_offer)
        end
        flash[:success] = 'Offer created'
        redirect '/job_offers/my'
      else
        flash.now[:error] = 'Mandatory fields are not setted or wrong values'
        render 'job_offers/new'
      end
    end  
  end

  post :update, :with => :offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_offer.update(params[:job_offer])

    error = nil

    expiration_date = params[:job_offer][:expiration_date]
    unless JobOfferExpirationPolicy.valid_expiration_date?(expiration_date)
      error = 'Invalid expiration date. Must be greater equal than current date'
    end

    years_required = params[:job_offer][:years_required]
    unless JobOfferYearsRequiredPolicy.valid_years_required?(years_required)
      error = 'Years required must be numeric and greater or equal than zero'
    end

    if error
      flash.now[:error] = error
      render 'job_offers/edit'
    else
      if @job_offer.save
        flash[:success] = 'Offer updated'
        redirect '/job_offers/my'
      else
        flash.now[:error] = 'Mandatory fields are not setted or wrong values'
        render 'job_offers/edit'
      end  
    end
  end

  put :activate, :with => :offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_offer.activate
    if @job_offer.save
      flash[:success] = 'Offer activated'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Operation failed'
      redirect '/job_offers/my'
    end  
  end

  put :republish, :with => :offer_id do
    @job_offer = JobOffer.get(params[:offer_id])
    @job_offer.republish
    if @job_offer.save
      flash[:success] = 'Offer republished'
      redirect '/job_offers/myall'
    else
      flash.now[:error] = 'Operation failed'
      redirect '/job_offers/myall'
    end  
  end

  delete :destroy do
    @job_offer = JobOffer.get(params[:offer_id])
    if @job_offer.destroy
      flash[:success] = 'Offer deleted'
    else
      flash.now[:error] = 'Couldnt delete offer'
    end
    redirect 'job_offers/my'
  end

end
