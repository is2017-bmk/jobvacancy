Feature: Offer expiration date
  As job vacancy owner
  I want that every new offer created has expiration date
  In order to keep only active offers

  Background:
    Given I am logged in as job offerer

  Scenario: Create new offer with invalid expiration date
    Given I access the new offer page
    When I fill the title with "Programmer vacancy"
    And  I fill the expiration date with "1990-12-30"
    And confirm the new offer
    Then ​I receive the error "Invalid expiration date. Must be greater equal than current date"

  Scenario: Update offer with invalid expiration date
    Given I have "Programmer vacancy" offer in My Offers
    And I edit it
    And  I fill the expiration date with "1990-12-30"
    And I save the modification
    Then ​I receive the error "Invalid expiration date. Must be greater equal than current date"

  Scenario: Create new offer with future expiration date
    Given I access the new offer page
    When I fill the title with "Programmer vacancy"
    And  I fill the expiration date with a future date
    And confirm the new offer
    Then I should see "Offer created"
    And I should see "Programmer vacancy" in My Offers

  Scenario: Create new offer with current
    Given I access the new offer page
    When I fill the title with "Programmer vacancy"
    And  I fill the expiration date with current date
    And confirm the new offer
    Then I should see "Offer created"
    And I should see "Programmer vacancy" in My Offers

  Scenario: offer inactive by expiration date not visible on My Offers
    Given I have "Cobol Bad Dev" offer in My Offers with current expiration date
    When deactivate offers jobs run
    And I should not see "Cobol Bad Dev" in My Offers

  Scenario: offer active after jobs run
    Given I have "Cobol Cool Dev" offer in My Offers with future expiration date
    When deactivate offers jobs run
    And I should see "Cobol Cool Dev" in My Offers

  Scenario: offer inactive by expiration date visible on View All Offers
    Given I have "Python Bad Dev" offer in My Offers with current expiration date
    When deactivate offers jobs run
    Then I should see "Python Bad Dev" in View All Offers