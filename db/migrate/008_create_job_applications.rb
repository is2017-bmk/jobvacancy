migration 8, :create_job_applications do
  up do
    create_table :job_applications do
      column :id, Integer, serial: true
      column :applicant_email, DataMapper::Property::String, length: 255
      column :applicant_short_bio, DataMapper::Property::Text
      column :created_on, DateTime
      column :job_offer_id, Integer
    end
  end

  down do
    drop_table :job_applications
  end
end
