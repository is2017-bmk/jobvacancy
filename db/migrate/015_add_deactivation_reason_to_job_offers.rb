migration 15, :add_finalization_reason_to_job_offers do
  up do
    modify_table :job_offers do
      add_column :finalization_reason, String
    end
  end

  down do
    modify_table :job_offers do
      drop_column :finalization_reason
    end
  end
end
