migration 14, :add_failed_attempts_to_users do
  up do
  	 modify_table :users do
      add_column :failed_attempts, Integer, :default => 0
    end
  end

  down do
  	modify_table :users do
      drop_column :failed_attempts
    end
  end
end